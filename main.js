let priceField = document.createElement('input');
priceField.type = 'number';
let wrapper = document.createElement('div');
$(wrapper).prependTo('body');

let name = document.createElement('p');
name.innerText = 'Price,$:';
$(name).css({
    'color': 'white',
    display: 'inline-block',
    'padding-right': '3px'
});

$(name).appendTo(wrapper);
$(priceField).appendTo(wrapper);

$('body').css({
    'display': 'flex',
    'justifyContent': 'center',
    'alignItems': 'center',
    'flexDirection': 'column'
});

$(wrapper).css({
    'padding': '10px',
    'backgroundColor': '#ff9900',
    'borderRadius': '5px',
    'marginBottom': '5px',
    'marginTop': '5px'
});

$(priceField).css({
    'autoFocus': 'false',
    'boxShadow': 'none',
    'width': '100px',
    'outline': 'none'
});

$(priceField).focus(function () {
    $(priceField).css('outline', '2px solid green');
});


let wrongPrice = document.createElement('p');
$(wrapper).after(wrongPrice);
wrongPrice.innerText = 'Please, enter correct price';
$(wrongPrice).css({
    'border': '1px solid grey',
    'borderRadius': '5px',
    'backgroundColor': '#f0f0f5',
    'display': 'none',
    'padding': '5px'
});

$(priceField).blur(function () {
    $(priceField).css('outline', 'none');
    let $priceVal = $(priceField).val();
    corrPrice.innerText = 'Текущая цена: $' + `${$priceVal}`;
    if ($priceVal < 0) {
        $(priceField).css('outline', '2px solid red');
        $(priceField).css('color', 'red');
        $(corrPrice).hide();
        $(wrongPrice).show();
    } else {
        $(wrongPrice).hide();
        $(corrPrice).show();
        $(close).appendTo(corrPrice);
        $(priceField).css('color', 'green');
    }
});

let corrPrice = document.createElement('span');
$(corrPrice).prependTo('body');
let close = document.createElement('p');
close.innerText = 'x';


$(corrPrice).css({
    'border': '1px solid grey',
    'borderRadius': '5px',
    'backgroundColor': '#f0f0f5',
    'display': 'none',
    'padding': '5px',
    'position': 'relative'
});

$(close).css({
    'borderRadius': '50%',
    'color': 'grey',
    'backgroundColor': '#f0f0f5',
    'padding': '3px 5px',
    'position': 'absolute',
    'top': '-21px',
    'right': '-14px',
    'cursor': 'pointer'
});

$(close).click(function () {
    $(corrPrice).hide();
    priceField.value = '';
});